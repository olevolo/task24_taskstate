package com.epam;

import com.epam.view.View;

public class Application {
    public static void main(String[] args) {
        View view = new View();
        view.show();
    }
}
