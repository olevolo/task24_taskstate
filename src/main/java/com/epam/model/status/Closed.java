package com.epam.model.status;

import com.epam.model.Task;

public class Closed implements Status {

  public void open(Task task){
    System.out.println("Task " + task.getDescription() + "is opened again");
    task.setStatus(new Open());
  }

  public void getInProgress(Task task) {
    System.out.println("Task ");
  }

  public void reopen(Task task) {
    System.out.println("Task " + task.getDescription() + "cannot be reopened");
  }

  public void resolve(Task task) {
    System.out.println("Task " + task.getDescription() + "cannot be resolved");
  }

  public void close(Task task) {
    System.out.println("Task " + task.getDescription() + "is already opened");
    task.setStatus(this);
  }
}
