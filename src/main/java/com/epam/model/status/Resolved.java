package com.epam.model.status;

import com.epam.model.Task;

public class Resolved implements Status {

  public void open(Task task){
    System.out.println("Task " + task.getDescription() + "cannot be opened");
  }

  public void getInProgress(Task task) {
    System.out.println("Task " + task.getDescription() + "cannot be in progress");
  }

  public void reopen(Task task) {
    System.out.println("Task " + task.getDescription() + " is reopened now ");
    task.setStatus(new Reopened());
  }

  public void resolve(Task task) {
    System.out.println("Task " + task.getDescription() + "is already resolved");
    task.setStatus(this);
  }

  public void close(Task task) {
    System.out.println("Task " + task.getDescription() + " is closed");
    task.setStatus(new Closed());
  }
}
