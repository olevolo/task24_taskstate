package com.epam.model.status;

import com.epam.model.Task;

public class Open implements Status {

  public void open(Task task) {
    System.out.println("Open task can not be reopened");
    task.setStatus(this);
  }

  public void getInProgress(Task task) {
    System.out.println("Task" + task.getDescription() + " is in progress now ");
    task.setStatus(new InProgress());
  }

  public void reopen(Task task) {
    System.out.println("Open task cannot be reopened");
  }

  public void resolve(Task task) {
    System.out.println("Open task cannot be resolved");
  }

  public void close(Task task) {
    System.out.println("Task " + task.getDescription() + " is closed now ");
    task.setStatus(new Closed());
  }

}
