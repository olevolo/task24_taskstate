package com.epam.model.status;

import com.epam.model.Task;

public interface Status {

    void open(Task task);

    void getInProgress(Task task);

    void reopen(Task task);

    void resolve(Task task);

    void close(Task task);

}
