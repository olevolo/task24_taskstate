package com.epam.model.status;

import com.epam.model.Task;

public class InProgress implements Status {

  public void open(Task task){
    System.out.println("Task " + task.getDescription() + " is opened again ");
    task.setStatus(new Open());
  }

  public void getInProgress(Task task) {
    System.out.println("Task " + task.getDescription() + " is already in progress");
    task.setStatus(this);
  }

  public void reopen(Task task) {
    System.out.println("Task " + task.getDescription() + "can not be reopened");
  }

  public void resolve(Task task)
  {
    System.out.println("Task " + task.getDescription() + "is now resolved");
    task.setStatus(new Resolved());
  }

  public void close(Task task) {
    System.out.println("Task " + task.getDescription() + "cannot be closed");
  }


}
