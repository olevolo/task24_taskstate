package com.epam.model.status;

import com.epam.model.Task;

public class Reopened implements Status {

  public void open(Task task) {
    System.out.println("Task " + task.getDescription() + "cannot be opened");
  }

  public void getInProgress(Task task) {
    System.out.println("Task is in progress now");
    task.setStatus(new InProgress());
  }

  public void reopen(Task task) {
    System.out.println("Task " + task.getDescription() + "is already reopened");
    task.setStatus(this);
  }

  public void resolve(Task task) {
    System.out.println("Task " + task.getDescription() + "cannot be resolved");
  }

  public void close(Task task) {
    System.out.println("Task " + task.getDescription() + "cannot be closed");
  }

}
