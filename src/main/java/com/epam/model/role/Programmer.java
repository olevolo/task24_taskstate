package com.epam.model.role;

import com.epam.model.Task;

public class Programmer implements User {
    Task task;

    public Programmer() {
    }

    public void receiveTask(Task task) {
        this.task = task;
    }
    public void showTask() {
        System.out.println(task);
    }

    public void resolveTask() {
        System.out.println("Programmer: That was hard, but I did it!");
        task.resolve();
    }
}
