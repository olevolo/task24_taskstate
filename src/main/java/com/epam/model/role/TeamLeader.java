package com.epam.model.role;

import com.epam.model.Task;
import com.epam.model.status.Open;

public class TeamLeader implements User {

    private Task task;

    public TeamLeader() {}

    public Task getTask() { return task; }
    public void setTask(Task task) { this.task = task; }

    /* create new task and set its status to open */
    public void createTask(String description) {
        System.out.println("I create so cool tasks!");
        task = new Task(description, new Open());
    }

    /* assign task to programmer and set state InProgress*/
    public void assignTask() {
        System.out.println("TeamLeader: I suppose programmer is going to have a lot of work!");
        task.getInProgress();
    }

    public void closeTask() {
        System.out.println("TeamLeader: Suppose task is completed. Time to close!");
        task.close();
    }

    public void showTask() {
        System.out.println(task);
    }
}
