package com.epam.model;

import com.epam.model.status.Open;
import com.epam.model.status.Status;

public class Task {
    private static int ID_COUNTER = 0;
    private int id;
    private String description;
    private Status status;

    {
        this.id = ++ID_COUNTER;
        this.status = new Open();
    }

    public Task() {

    }
    public Task(String description, Status status) {
        this.description = description;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public Status getStatus() {
        return status;
    }
    public void setStatus(Status status) {
        this.status = status;
    }

    public String getDescription() { return description; }

    public void setDescription(String description) { this.description = description; }

    public void open() {
        status.open(this);
    }

    public void getInProgress() {
        status.getInProgress(this);
    }

    public void reopen() {
        status.reopen(this);
    }

    public void resolve() {
        status.resolve(this);
    }

    public void close() {
        status.close(this);
    }

    @Override
    public String toString() {
        return "Task:" +
                "\nid: " + id +
                "\ndescription: " + description +
                "\nstatus: " + getStatus().getClass().getSimpleName().toLowerCase();
    }
}
