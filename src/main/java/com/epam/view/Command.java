package com.epam.view;

@FunctionalInterface
public interface Command {
    void execute();
}

