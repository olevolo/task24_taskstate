package com.epam.view;

import com.epam.model.role.Programmer;
import com.epam.model.role.TeamLeader;
import com.epam.model.role.User;
import com.epam.model.role.UserFactory;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View {

  private Map<String, String> menu;
  private Map<String, Command> methodsMenu;
  private static Scanner input = new Scanner(System.in, "utf-8");
  //private User user;
  private TeamLeader teamLeader = new TeamLeader();
  private Programmer programmer = new Programmer();

  public View() {
    menu = new LinkedHashMap<>();
    methodsMenu = new LinkedHashMap<>();
    setLoginMenu();

  }

  private void login() {
    System.out.print("Login: ");
    String login = input.nextLine();

    //user = new UserFactory().createUser(login);
    defineUserMenu(login);
  }

  private void logout() {
      System.out.println("Logging out...");
      setLoginMenu();
  }
  private void exit() {
      System.out.println("Closing application...");
      System.exit(0);
  }

  private void defineUserMenu(String login) {
    if (login.contains("admin")) setTeamLeaderMenu();
    else setProgrammerMenu();
    show();
  }

  private void setLoginMenu() {
      menu.clear();
      methodsMenu.clear();

      menu.put("1", "  1 - Log in");
      menu.put("E", "  E - Exit application");

      methodsMenu.put("1", this::login);
      methodsMenu.put("2", this::exit);
  }
  private void setTeamLeaderMenu() {
    menu.clear();
    methodsMenu.clear();

    menu.put("1", "  1 - Create new task");
    menu.put("2", "  2 - Assign task to programmer");
    menu.put("3", "  3 - Close task");
    menu.put("4", "  4 - Show task status");
    menu.put("5", "  5 - Reopen task");
    menu.put("Q", "  Q - Log out");
    menu.put("E", "  E - Exit application");

    methodsMenu.put("1", this::createTask);
    methodsMenu.put("2", teamLeader::assignTask);
    methodsMenu.put("3", teamLeader::closeTask);
    methodsMenu.put("4", teamLeader::showTask);
    //
    methodsMenu.put("Q", this::logout);
    methodsMenu.put("E", this::exit);
  }

    private void setProgrammerMenu() {
        menu.clear();
        methodsMenu.clear();

        menu.put("1", "  1 - Show task");
        menu.put("2", "  2 - Resolve task");
        menu.put("Q", "  Q - Log out");
        menu.put("E", "  E - Exit application");

        methodsMenu.put("1", programmer::showTask);
        methodsMenu.put("2", programmer::resolveTask);
        methodsMenu.put("Q", this::logout);
        methodsMenu.put("E", this::exit);
    }

  private void outputMenu() {
    System.out.println("MENU:");
    for (String str : menu.values()) {
      System.out.println(str);
    }
  }

  public void show() {
    String keyMenu;
    do {
      outputMenu();
      System.out.print("Please, select menu point: ");
      keyMenu = input.nextLine().toUpperCase();
      methodsMenu.get(keyMenu).execute();
    } while (!keyMenu.equals("E"));
  }

  private void createTask() {
      System.out.print("Write short description for task: ");
      teamLeader.createTask(input.nextLine());
      programmer.receiveTask(teamLeader.getTask());
  }


}
